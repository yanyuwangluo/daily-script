# -*- coding: utf-8 -*-
"""
-------------------------------------------------
   File Name：    xm.py
   Description :
   Author :       烟雨
   date：        2022/12/12 17:34
   @Ver :        0.0.1
   @software:    vscode
-------------------------------------------------
   Change Activity:
                   2022/12/12 17:34
-------------------------------------------------
"""

"""
cron: 05 9 * * *
new Env('小米运动（邮箱版）');
"""
"""
使用方法：
    拉取脚本，设置环境变量xmbse，值为‘账号-密码-步数’
    仅支持青龙面板，此脚本支持多账户执行,例如：export xmbse='账号-密码-步数#账号-密码-步数'
"""
import json
import os
import requests
try:
    xmbse = os.environ.get('xmbse').split("#")
except Exception as e:
    print(e)
    print('未查询到xmbse,请确认是否设置xmbse环境变量')
else:
    if xmbse == None:
        print('未查询到xmbse环境变量,退出！')
    else:
        n = 0
        for xmbse in list(xmbse):
            n = n + 1
            print("\n正在执行第" + "%d" % (n) + "个叼毛")
            try:
                phone, password, step = xmbse.split("-")
            except Exception as e:
                print(e)
                print('phone, password,step格式错误！')
            else:
                url = "https://api.kit9.cn/api/xiaomi_sports/api_email_fixed.php?email=" + \
                    phone + "&password=" + password + "&step=" + step
                response = requests.get(url)
                #判断返回中的code是否为200
                if response.json()['code'] == 200:
                    print("返回状态："+response.json()['msg'])
                    print("刷步成功")
                else:
                    print("返回状态："+response.json()['msg'])
                    print("刷步失败")

